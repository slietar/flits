<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.1" name="TS0" tilewidth="16" tileheight="16" tilecount="100" columns="10">
 <editorsettings>
  <export target="ts0.json" format="json"/>
 </editorsettings>
 <image source="ts0.png" width="160" height="160"/>
 <tile id="0">
  <properties>
   <property name="spawn" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="spawn" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="flag" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="flag" type="int" value="1"/>
  </properties>
 </tile>
</tileset>
