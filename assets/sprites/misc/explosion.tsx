<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.1" name="Explosion" tilewidth="20" tileheight="20" tilecount="7" columns="7">
 <image source="explosion.png" width="140" height="20"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="120"/>
   <frame tileid="1" duration="120"/>
   <frame tileid="2" duration="120"/>
   <frame tileid="3" duration="120"/>
   <frame tileid="4" duration="120"/>
   <frame tileid="5" duration="120"/>
   <frame tileid="6" duration="120"/>
  </animation>
 </tile>
</tileset>
