<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.1" name="Impact" tilewidth="20" tileheight="20" tilecount="4" columns="4">
 <editorsettings>
  <export target="impact.json" format="json"/>
 </editorsettings>
 <image source="impact.png" width="80" height="20"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="80"/>
   <frame tileid="1" duration="80"/>
   <frame tileid="2" duration="80"/>
   <frame tileid="3" duration="80"/>
  </animation>
 </tile>
</tileset>
