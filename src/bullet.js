import planck from 'planck-js';

import { ParticleAnimation } from './particle';
import * as util from './util';


export class Bullet {
  constructor(options) {
    this.game = options.game;
    this.image = options.resources.bulletImage;
    this.physScale = options.physScale;
    this.resources = options.resources;
    this.world = options.world;

    this.body = options.world.createDynamicBody({
      allowSleep: false,
      bullet: true,
      fixedRotation: true,
      gravityScale: 0,
      linearVelocity: planck.Vec2(options.velocity.x, options.velocity.y),
      position: planck.Vec2(options.position.x, options.position.y)
    });


    let hitbox = options.hitbox || {
      width: this.image.width * options.physScale * 0.1,
      height: this.image.height * options.physScale * 0.1
    };

    // hits walls
    this.body.createFixture({
      filterCategoryBits: 0b100,
      filterMaskBits: 0b1,
      shape: planck.Box(hitbox.width / 2, hitbox.height / 2)
    });

    // hits characters
    this.body.createFixture({
      filterCategoryBits: 0b100,
      filterMaskBits: 0b10,
      isSensor: true,
      shape: planck.Box(hitbox.width / 2, hitbox.height / 2)
    });
  }

  destroy() {
    this.world.destroyBody(this.body);
  }

  hitGround(position) {
    setTimeout(() => {
      this.destroy();
    });

    this.game.particles.add(
      new ParticleAnimation({
        game: this.game,
        position, // : this.body.getPosition(),
        physScale: this.physScale,
        tileset: this.resources.impactTileset
      })
    );

    if (this.game.post) {
      this.game.post.shake(2);
    }

    this.game.playAudio(this.resources.wallHitAudio);

    for (let character of this.game.characters) {
      let controller = character.controller;

      if (controller) {
        controller.vibrate(0.3, 200);
      }
    }
  }

  hitCharacter(position) {
    setTimeout(() => {
      this.world.destroyBody(this.body);
    });
  }

  render(ctx) {
    let position = this.body.getPosition();
    let targetX = position.x / this.physScale - this.image.width / 2;
    let targetY = position.y / this.physScale - this.image.height / 2;

    ctx.drawImage(this.image,
      0, 0,
      this.image.width, this.image.height,
      targetX, targetY,
      this.image.width, this.image.height
    );

    if (this.game.renderSettings.debug) {
      util.renderFixture(ctx, this.body.getFixtureList(), this.physScale, 'green');
    }
  }

  update(dt) {

  }
}
