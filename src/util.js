export function chooseRandomly(arr) {
  let index = Math.floor(Math.random() * arr.length);
  return arr[index];
}


export function loadAudio(url) {
  let audio = new Audio(url);

  return new Promise((resolve) => {
    audio.addEventListener('canplaythrough', () => {
      resolve(audio);
    });
  });
}


export function loadImage(url) {
  let image = new Image();
  image.src = url;

  return new Promise((resolve) => {
    image.addEventListener('load', () => {
      resolve(image);
    });
  });
}


export async function loadResources(obj) {
  let resources = {};

  await Promise.all(
    Object.entries(obj).map(async ([name, promise]) => {
      resources[name] = await promise;
    })
  );

  return resources;
}


export function renderFixture(ctx, fixture, physScale, color = 'rgba(0, 0, 0, 0.2)') {
  let type = fixture.getType();
  let shape = fixture.getShape();

  let pos = fixture.getBody().getPosition();

  if (type === 'polygon') {
    let vertices = shape.m_vertices;

    ctx.save();
    ctx.scale(1 / physScale, 1 / physScale);
    ctx.translate(pos.x, pos.y);

    ctx.beginPath();
    ctx.moveTo(vertices[0].x, vertices[0].y);

    for (let index = 1; index < vertices.length; index++) {
      let vertex = vertices[index];
      ctx.lineTo(vertex.x, vertex.y);
    }

    ctx.closePath();

    ctx.lineWidth = 1 * physScale;
    ctx.strokeStyle = color;
    ctx.stroke();

    ctx.restore();
  }
}
