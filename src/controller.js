class Controller {
  start(response) {
    let old = {
      left: false,
      up: false,
      right: false,
      down: false,
      fire: false
    };

    this.old = old;

    this.startContinuous({
      left() {
        if (!old.left) {
          old.left = true;
          response.left();
        }
      },
      up() {
        if (!old.up) {
          old.up = true;
          response.up();
        }
      },
      right() {
        if (!old.right) {
          old.right = true;
          response.right();
        }
      },
      down() {
        if (!old.down) {
          old.down = true;
          response.down();
        }
      },
      fire() {
        if (!old.fire) {
          old.fire = true;
          response.fire();
        }
      },

      leftEnd() {
        if (old.left) {
          old.left = false;
          response.leftEnd();
        }
      },
      upEnd() {
        if (old.up) {
          old.up = false;
          response.upEnd();
        }
      },
      rightEnd() {
        if (old.right) {
          old.right = false;
          response.rightEnd();
        }
      },
      downEnd() {
        if (old.down) {
          old.down = false;
          response.downEnd();
        }
      },
      fireEnd() {
        if (old.fire) {
          old.fire = false;
          response.fireEnd();
        }
      }
    });
  }

  vibrate(magniture, duration) {

  }
}


export class KeyboardController extends Controller {
  constructor(keys) {
    super();

    this._keys = keys;
    this._keydownListener = null;
    this._keyupListener = null;
  }

  startContinuous(response) {
    this._keydownListener = (event) => {
      switch (event.key) {
        case this._keys.left:
          response.left();
          break;
        case this._keys.up:
          response.up();
          break;
        case this._keys.right:
          response.right();
          break;
        case this._keys.down:
          response.down();
          break;
        case this._keys.fire:
          response.fire();
          break;
        default:
          return;
      }

      event.preventDefault();
    };

    this._keyupListener = (event) => {
      switch (event.key) {
        case this._keys.left:
          response.leftEnd();
          break;
        case this._keys.up:
          response.upEnd();
          break;
        case this._keys.right:
          response.rightEnd();
          break;
        case this._keys.down:
          response.downEnd();
          break;
        case this._keys.fire:
          response.fireEnd();
          break;
        default:
          return;
      }

      event.preventDefault();
    };

    window.addEventListener('keydown', this._keydownListener);
    window.addEventListener('keyup', this._keyupListener);
  }

  stop() {
    window.removeEventListener('keydown', this._keydownListener);
    window.removeEventListener('keyup', this._keyupListener);

    this._keydownListener = null;
    this._keyupListener = null;
  }
}

export class ArrowsController extends KeyboardController {
  constructor() {
    super({
      left: 'ArrowLeft',
      up: 'ArrowUp',
      right: 'ArrowRight',
      down: 'ArrowDown',
      fire: '='
    });
  }
}

export class ZQSDController extends KeyboardController {
  constructor() {
    super({
      left: 'q',
      up: 'z',
      right: 'd',
      down: 's',
      fire: ' '
    });
  }
}

export class WASDController extends KeyboardController {
  constructor() {
    super({
      left: 'a',
      up: 'w',
      right: 'd',
      down: 's',
      fire: ' '
    });
  }
}


export class GamepadController extends Controller {
  constructor(gamepadIndex, disconnectCallback) {
    super();

    this._disconnectCallback = disconnectCallback;
    this._enabled = false;
    this._gamepadIndex = gamepadIndex;
  }

  get gamepad() {
    return Array.from(navigator.getGamepads())
      .find((gp) => gp && gp.index === this._gamepadIndex);
  }

  startContinuous(response) {
    let handler = () => {
      let gamepad = this.gamepad;

      if (!gamepad) {
        this._disconnectCallback();
        return;
      }

      // X = 0, O = 1, square = 2, triangle = 3

      let left = gamepad.axes[0] < -0.4;
      let up = gamepad.buttons[0].pressed;
      let right = gamepad.axes[0] > 0.4;
      let down = false;
      let fire = gamepad.buttons[7].pressed;

      if (left) response.left();
      else response.leftEnd();

      if (up) response.up();
      else response.upEnd();

      if (right) response.right();
      else response.rightEnd();

      if (down) response.down();
      else response.downEnd();

      if (fire) response.fire();
      else response.fireEnd();

      if (this._enabled) {
        requestAnimationFrame(handler);
      }
    };

    this._enabled = true;
    requestAnimationFrame(handler);
  }

  stop() {
    this._enabled = false;
  }


  vibrate(magnitude, duration) {
    let gamepad = this.gamepad;

    if (gamepad && gamepad.vibrationActuator) {
      gamepad.vibrationActuator.playEffect('dual-rumble', {
        startDelay: 0,
        duration,
        weakMagnitude: magnitude,
        strongMagnitude: magnitude
      });
    }
  }
}
