import planck from 'planck-js';

import { Flag, FlagBase } from './flag';
import * as util from './util';


export class GameMap {
  constructor(options) {
    this.game = options.game;
    this.layers = options.layers;
    this.size = options.size;
    this.tileSize = options.tileSize;
    this.tilesets = options.tilesets;

    this.physScale = options.physScale;
    this.world = options.world;


    this.body = options.world.createBody();
    this.fixtures = new Set();

    for (let layer of this.layers) {
      for (let y = 0; y < this.size.height; y++) {
        let startX = -1;

        for (let x = 0; x <= this.size.width; x++) {
          let index = y * this.size.width + x;

          let collides;

          if (x === this.size.width) {
            collides = false;
          } else {
            let value = layer.data[index];

            if (value === 0) {
              collides = false;
            } else {
              let tileset = this.getTileset(value);
              let tile = tileset.tileset.tiles[value - tileset.firstgid];

              collides = tile.collides || false;
            }
          }

          if (collides) {
            if (startX < 0) {
              startX = x;
            }
          } else {
            if (startX >= 0) {
              let fixture = this.body.createFixture({
                filterCategoryBits: 0b1,
                filterMaskBits: 0b110,
                friction: 0,
                shape: planck.Box(
                  this.tileSize.width * (x - startX) / 2 * this.physScale,
                  this.tileSize.height / 2 * this.physScale,
                  planck.Vec2(
                    (startX + (x - startX) / 2) * this.tileSize.width * this.physScale,
                    (y + 0.5) * this.tileSize.height * this.physScale
                  )
                )
              });

              this.fixtures.add(fixture);
            }

            startX = -1;
          }
        }
      }
    }


    let metaLayer = this.layers.find((layer) => layer.name === 'Meta');

    this.flags = [];
    this.flagBases = [];
    this.spawnPoints = [];

    if (metaLayer) {
      for (let y = 0; y < this.size.height; y++) {
        for (let x = 0; x < this.size.width; x++) {
          let index = y * this.size.width + x;
          let value = metaLayer.data[index];

          if (value === 0) {
            continue;
          }

          let tileset = this.getTileset(value);
          let tile = tileset.tileset.tiles[value - tileset.firstgid];

          let position = {
            x: (x + 0.5) * this.tileSize.width * this.physScale,
            y: (y + 0.5) * this.tileSize.height * this.physScale
          };

          if (tile.flag !== void 0) {
            this.flags.push(new Flag({
              game: this.game,
              key: tile.flag,
              physScale: this.physScale,
              spawnPoint: position,
              tileset: options.resources.flagTilesets[tile.flag],
              world: this.world
            }));

            this.flagBases.push(new FlagBase({
              key: tile.flag,
              position,
              world: this.world
            }));
          }

          if (tile.spawn !== void 0) {
            this.spawnPoints.push({
              ...position,
              key: tile.spawn
            });
          }
        }
      }
    }
  }

  get pixelSize() {
    return {
      width: this.size.width * this.tileSize.width,
      height: this.size.height * this.tileSize.height
    };
  }

  destroy() {
    for (let flag of this.flags) {
      flag.destroy();
    }

    this.world.destroyBody(this.body);
  }

  getTileset(value) {
    for (let tileset of this.tilesets) {
      if (value >= tileset.firstgid && value < tileset.firstgid + tileset.tileset.tileCount) {
        return tileset;
      }
    }

    return null;
  }


  reset() {
    for (let flag of this.flags) {
      flag.reset();
    }
  }


  render(ctx) {
    for (let layer of this.layers) {
      if (layer.name === 'Meta') {
        continue;
      }

      for (let y = 0; y < this.size.height; y++) {
        for (let x = 0; x < this.size.width; x++) {
          let index = y * this.size.width + x;
          let value = layer.data[index];

          if (value === 0) {
            continue;
          }

          let tileset = this.getTileset(value);

          let { image, x: imageX, y: imageY } = tileset.tileset.getTile(value - tileset.firstgid);
          let imageSize = tileset.tileset.tileSize;

          ctx.drawImage(image,
            imageX, imageY,
            imageSize.width, imageSize.height,
            x * this.tileSize.width,
            y * this.tileSize.height,
            this.tileSize.width,
            this.tileSize.height
          );
        }
      }
    }

    for (let flag of this.flags) {
      flag.render(ctx);
    }

    if (this.game.renderSettings.debug) {
      for (let fixture = this.body.getFixtureList(); fixture; fixture = fixture.getNext()) {
        util.renderFixture(ctx, fixture, this.physScale, 'rgba(255, 0, 0, 0.4)');
      }

      for (let flagBase of this.flagBases) {
        util.renderFixture(ctx, flagBase.body.getFixtureList(), this.physScale, 'pink');
      }
    }
  }


  static async load(mapUrlRaw, options) {
    let mapUrl = new URL(mapUrlRaw, self.location);
    let mapData = await (await fetch(mapUrl)).json();

    // for (let tilesetInfo of mapData.tilesets) {
    let tilesets = await Promise.all(
      mapData.tilesets.map(async (tilesetInfo) => {
        let tilesetUrl = new URL(tilesetInfo.source.slice(0, -3) + 'json', mapUrl);
        let tileset = await Tileset.load(tilesetUrl);

        return {
          firstgid: tilesetInfo.firstgid,
          tileset
        };
      })
    );

    return new GameMap({
      ...options,
      layers: mapData.layers
        .filter((layerData) => layerData.visible)
        .map((layerData) => ({
          data: layerData.data,
          name: layerData.name
        })),
      size: {
        width: mapData.width,
        height: mapData.height
      },
      tileSize: {
        width: mapData.tilewidth,
        height: mapData.tileheight
      },
      tilesets
    });
  }
}


export class Tileset {
  constructor(options) {
    this.animations = options.animations;
    this.image = options.image;
    this.name = options.name;
    this.tileSize = options.tileSize;
    this.size = options.size;

    this.tiles = [];

    for (let index = 0; index < this.tileCount; index++) {
      this.tiles[index] = options.tiles[index] || {};
    }
  }

  get tileCount() {
    return this.size.width * this.size.height;
  }

  getTile(index) {
    return {
      image: this.image,
      x: (index % this.size.width) * this.tileSize.width,
      y: Math.floor(index / this.size.width) * this.tileSize.height
    };
  }

  renderAnimation(options /* loop, position: center in pixel coords */) { /* -> isDone */
    let animation = this.animations[0];
    let initialTime = Date.now();

    return (ctx, position = options.position) => {
      let time = Date.now() - initialTime;
      let tileIndex = -1;

      if (options.loop) {
        time %= animation.duration;
      }

      for (let [frameTime, frameTileIndex] of animation.frames) {
        if (frameTime >= time) {
          tileIndex = frameTileIndex;
          break;
        }
      }

      if (tileIndex < 0) {
        return true;
      }

      let { image, x, y } = this.getTile(tileIndex);

      ctx.drawImage(
        image,
        x, y,
        this.tileSize.width,
        this.tileSize.height,
        position.x - this.tileSize.width / 2,
        position.y - this.tileSize.height / 2,
        this.tileSize.width,
        this.tileSize.height
      );

      return false;
    };
  }

  renderTile(ctx, index, position /* center */, factor = 1) {
    ctx.drawImage(
      this.image,
      (index % this.size.width) * this.tileSize.width,
      Math.floor(index / this.size.width) * this.tileSize.height,
      this.tileSize.width,
      this.tileSize.height,
      position.x - this.tileSize.width * factor / 2,
      position.y - this.tileSize.height * factor / 2,
      this.tileSize.width * factor,
      this.tileSize.height * factor
    );
  }


  static async load(tilesetUrlRaw) {
    let tilesetUrl = new URL(tilesetUrlRaw, self.location);
    let tilesetData = await (await fetch(tilesetUrl)).json();
    let image = await util.loadImage(new URL(tilesetData.image, tilesetUrl).href);

    let animations = [];
    let tiles = {};

    for (let tileData of (tilesetData.tiles || [])) {
      let tile = {};

      for (let propertyData of (tileData.properties || [])) {
        tile[propertyData.name] = propertyData.value;
      }

      tiles[tileData.id] = tile;

      if (tileData.animation) {
        let frames = new Map();
        let cumulatedDuration = 0;

        for (let frameData of tileData.animation) {
          cumulatedDuration += frameData.duration;
          frames.set(cumulatedDuration, frameData.tileid);
        }

        animations.push({
          duration: cumulatedDuration,
          frames
        });
      }
    }

    return new Tileset({
      animations,
      image,
      name: tilesetData.name,
      size: {
        width: tilesetData.imagewidth / tilesetData.tilewidth,
        height: tilesetData.imageheight / tilesetData.tileheight
      },
      tileSize: {
        width: tilesetData.tilewidth,
        height: tilesetData.tileheight
      },
      tiles
    });
  }
}
