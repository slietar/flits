import gpgl from 'gpgl';

export class PostProcessing {
  constructor(options) {
    let width = options.destination.width = options.source.width;
    let height = options.destination.height = options.source.height;

    this._amplitude = 0;
    this._game = options.game;
    this._sourceCanvas = options.source;

    this._render = gpgl({
      inputs: [
        { name: 'amplitude' },
        { name: 'radius', length: 2 },

        { name: 'flags', length: 2, size: 2 },
        { name: 'source', length: 4, size: [width, height] }
      ],
      output: options.destination,

      shader: `
      float random (vec2 st) {
        return fract(sin(dot(st.xy, vec2(12.9898, 78.233))) * 43758.5453123);
      }

      vec4 kernel(vec2 thread1, vec2 resolution) {
        float scale = ${options.scale.toFixed(4)};

        // shake
        vec2 thread = thread1 + (radius * 2.0 - 1.0) * vec2(amplitude) * scale;
        vec2 uv = thread / resolution;

        vec4 pixel = source(thread) * step(0.0, uv.x) * step(0.0, uv.y) * (1.0 - step(1.0, uv.x)) * (1.0 - step(1.0, uv.y));

        // noise
        pixel += vec4(1, 1, 1, 0) * (random(thread / resolution) * 2.0 - 1.0) * 0.01;

        // flags
        for (int i = 0; i < 2; i++) {
          vec2 pos = flags(float(i)) * scale;
          float dist = distance(pos, thread) / scale;

          pixel += vec4(1, 1, 1, 1) / max(0.05 + 0.1 * dist + 0.01 * pow(dist, 2.0), 4.0);
        }

        return pixel;
      }
      `
    });
  }

  render(dt) {
    this._amplitude = Math.max(this._amplitude - dt * 10, 0);

    let flagsData = [];

    for (let flag of this._game.map.flags) {
      flagsData.push(flag.renderPosition.x, flag.renderPosition.y, 0, 0);
    }

    this._render({
      amplitude: this._amplitude,
      radius: [Math.random(), Math.random()],

      flags: flagsData,
      source: this._sourceCanvas
    });
  }

  shake(amplitude) {
    this._amplitude = amplitude;
  }
}

