import * as Tweakpane from 'tweakpane';
import planck from 'planck-js';
import Stats from 'stats.js';

import Character from './character';
import { PostProcessing } from './postprocessing';
import { GameMap, Tileset } from './map';
import * as ctrl from './controller';
import * as util from './util';


let pane = new Tweakpane({
  expanded: false,
  title: 'Settings'
});

let stats = new Stats();
document.body.appendChild(stats.dom);


const Settings = {
  physScale: 1 / 20,

  bulletVelocity: 10, // m/s
  firingInterval: 0.3, // s
  gravity: 24, // m/s^2
  jumpVelocity: 12, // m/s
  runVelocity: 7 // m/s
};


class Game {
  constructor(options) {
    this.characters = [];
    this.controls = null;
    this.bullets = new Set();
    this.map = null;
    this.particles = new Set();

    this.post = null;
    this.primaryCanvas = null;
    this.outputCanvas = null;

    this.scoreTag = document.querySelector('.score');
    this.scores = [0, 0];
    this.resources = options.resources;

    this.renderSettings = {
      decimalScaling: false,
      debug: false,
      deviceScale: window.devicePixelRatio,
      maximumUpdateDt: 100,
      postprocessing: true,
      postprocessingScaling: true,
      smoothing: false
    };

    this.audioSettings = {
      musicVolume: 1,
      sfxVolume: 1
    };

    this.world = new planck.World({
      gravity: planck.Vec2(0, options.settings.gravity)
    });


    this.musicAudio = null;

    window.addEventListener('keydown', () => {
      this.musicAudio = this.resources.musicAudio.cloneNode();
      this.musicAudio.volume = this.audioSettings.musicVolume;
      this.musicAudio.play();

      this.musicAudio.addEventListener('ended', () => {
        this.musicAudio.currentTime = 0;
        this.musicAudio.play();
      });
    }, { once: true });
  }

  playAudio(audio) {
    let cloned = audio.cloneNode();

    cloned.volume = this.audioSettings.sfxVolume;
    cloned.play();
  }

  reset() {
    for (let character of this.characters) {
      character.reset();
    }

    for (let bullet of this.bullets) {
      bullet.destroy();
    }

    this.bullets.clear();
    this.particles.clear();
    this.map.reset();
  }

  setHealthbar(key, level) {
    document.querySelector(`.healthbar:nth-of-type(${key + 1})`).setAttribute('data-level', level);
  }

  win(key) {
    this.scores[key]++;
    this.reset();

    this.scoreTag.innerText = `${this.scores[0]} - ${this.scores[1]}`;
    this.resources.pointAudio.cloneNode().play();
  }


  setupMap(map) {
    if (this.map) {
      this.map.destroy();
    }

    this.map = map;

    for (let character of this.characters) {
      character.resetMap();
    }

    for (let bullet of this.bullets) {
      bullet.destroy();
    }

    this.bullets.clear();
    this.particles.clear();
  }


  setupRender() {
    let canvas1 = document.createElement('canvas');
    let ctx = canvas1.getContext('2d');

    let mapPixelSize = this.map.pixelSize;
    let mapRatio = mapPixelSize.width / mapPixelSize.height;
    let windowRatio = window.innerWidth / window.innerHeight;

    let deviceScale = this.renderSettings.deviceScale;

    let scale = mapRatio < windowRatio
      ? window.innerHeight / mapPixelSize.height
      : window.innerWidth / mapPixelSize.width;

    let perfScale = this.renderSettings.decimalScaling
      ? scale
      : Math.floor(scale);

    let remScale = this.renderSettings.postprocessingScaling
      ? scale / perfScale
      : 1;

    let outputWidth = mapPixelSize.width * perfScale;
    let outputHeight = mapPixelSize.height * perfScale;

    canvas1.width = outputWidth * deviceScale;
    canvas1.height = outputHeight * deviceScale;

    ctx.scale(perfScale, perfScale);
    ctx.scale(deviceScale, deviceScale);

    ctx.imageSmoothingEnabled = this.renderSettings.smoothing;

    let outputCanvas;

    if (this.renderSettings.postprocessing) {
      let canvas2 = document.createElement('canvas');
      canvas2.style.setProperty('transform', `scale(${remScale})`);

      document.body.appendChild(canvas2);

      this.post = new PostProcessing({
        source: canvas1,
        destination: canvas2,
        scale: perfScale * deviceScale,
        game
      });

      outputCanvas = canvas2;
    } else {
      outputCanvas = canvas1;
    }

    outputCanvas.style.setProperty('width', outputWidth + 'px');
    outputCanvas.style.setProperty('height', outputHeight + 'px');

    if (this.outputCanvas) {
      let parent = this.outputCanvas.parentElement;

      if (parent) {
        parent.replaceChild(outputCanvas, this.outputCanvas);
      }
    }

    this.primaryCanvas = canvas1;
    this.outputCanvas = outputCanvas;

    return outputCanvas;
  }

  render(dt) {
    let canvas = this.primaryCanvas;
    let ctx = canvas.getContext('2d');

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    this.map.render(ctx);

    for (let character of this.characters) {
      character.render(ctx);
    }

    for (let bullet of this.bullets) {
      bullet.render(ctx);
    }

    for (let particle of this.particles) {
      particle.render(ctx);
    }


    if (this.post) {
      this.post.render(dt / 1e3);
    }
  }

  update(dt) {
    this.world.step(dt);

    for (let character of this.characters) {
      character.update(dt);
    }

    for (let bullet of this.bullets) {
      bullet.update(dt);
    }
  }
}


main().catch((err) => {
  console.error(err);
});


async function main() {
  let resources = await util.loadResources({
    bulletImage: util.loadImage('./assets/sprites/misc/bullet.png'),
    explosionTileset: Tileset.load('./assets/sprites/misc/explosion.json'),
    fireAudio: util.loadAudio('./assets/sfx/fire.mp3'),
    flagTilesets: Promise.all([
      Tileset.load('./assets/sprites/flag/green.json'),
      Tileset.load('./assets/sprites/flag/yellow.json')
    ]),
    idleTilesetGreen: Tileset.load('./assets/sprites/character/idle_green.json'),
    idleTilesetYellow: Tileset.load('./assets/sprites/character/idle_yellow.json'),
    impactTileset: Tileset.load('./assets/sprites/misc/impact.json'),
    jumpAudio: util.loadAudio('./assets/sfx/jump.mp3'),
    jumpTilesetGreen: Tileset.load('./assets/sprites/character/jump_green.json'),
    jumpTilesetYellow: Tileset.load('./assets/sprites/character/jump_yellow.json'),
    musicAudio: util.loadAudio('./assets/sfx/music.mp3'),
    pickupFlagAudio: util.loadAudio('./assets/sfx/pickup_flag.mp3'),
    pointAudio: util.loadAudio('./assets/sfx/point.mp3'),
    playerDeathAudio: util.loadAudio('./assets/sfx/player_death.mp3'),
    runTilesetGreen: Tileset.load('./assets/sprites/character/run_green.json'),
    runTilesetYellow: Tileset.load('./assets/sprites/character/run_yellow.json'),
    wallHitAudio: util.loadAudio('./assets/sfx/wall_hit.mp3')
  });


  // -- Game --

  let game = new Game({
    resources,
    settings: Settings
  });
  self.game = game;


  // -- Character --

  let player1 = new Character({
    game,
    key: 0,
    physScale: Settings.physScale,
    resources,
    settings: Settings,
    tilesets: {
      idle: resources.idleTilesetGreen,
      jump: resources.jumpTilesetGreen,
      run: resources.runTilesetGreen
    },
    world: game.world
  });

  game.characters.push(player1);


  let player2 = new Character({
    game,
    key: 1,
    physScale: Settings.physScale,
    resources,
    settings: Settings,
    tilesets: {
      idle: resources.idleTilesetYellow,
      jump: resources.jumpTilesetYellow,
      run: resources.runTilesetYellow
    },
    world: game.world
  });

  game.characters.push(player2);


  // -- Map --

  let mapPaths = [
    './assets/maps/map0.json',
    './assets/maps/map1.json',
    './assets/maps/map2.json'
  ];

  let map = await GameMap.load(mapPaths[0], {
    game,
    physScale: Settings.physScale,
    resources,
    world: game.world
  });

  game.setupMap(map);


  let findBullet = (bodyA, bodyB) => {
    for (let bullet of game.bullets) {
      if (bullet.body === bodyA || bullet.body === bodyB) {
        return bullet;
      }
    }
  };

  let findCharacter = (bodyA, bodyB) => {
    for (let character of game.characters) {
      if (character.body === bodyA || character.body === bodyB) {
        return character;
      }
    }
  };

  let findFlag = (bodyA, bodyB) => {
    for (let flag of game.map.flags) {
      if (flag.body === bodyA || flag.body === bodyB) {
        return flag;
      }
    }
  };

  let findFlagBase = (bodyA, bodyB) => {
    for (let flagBase of game.map.flagBases) {
      if (flagBase.body === bodyA || flagBase.body === bodyB) {
        return flagBase;
      }
    }
  };

  game.world.on('begin-contact', (contact) => {
    let bodyA = contact.getFixtureA().getBody();
    let bodyB = contact.getFixtureB().getBody();

    let isGround = bodyA === map.body || bodyB === map.body;

    let bullet = findBullet(bodyA, bodyB);

    if (isGround) {
      if (bullet) {
        let manifold = contact.getWorldManifold();
        let point = manifold.points[0];

        bullet.hitGround({
          x: point.x,
          y: point.y
        });

        game.bullets.delete(bullet);

        return;
      }

      let character = findCharacter(bodyA, bodyB);

      if (character) {
        character.beginContact(contact);
        return;
      }
    }

    let character = findCharacter(bodyA, bodyB);

    if (bullet && character) {
      bullet.hitCharacter();
      character.hit();
      game.bullets.delete(bullet);

      return;
    }

    let flag = findFlag(bodyA, bodyB);

    if (flag && character) {
      if (flag.key == character.key) {
        setTimeout(() => {
          flag.reset();
        });
      } else {
        flag.setCarrier(character);
        character.takeFlag(flag);

        return;
      }
    }

    let flagBase = findFlagBase(bodyA, bodyB);

    if (flagBase && character) {
      if (character.key === flagBase.key && character.carriedFlag) {
        setTimeout(() => {
          game.win(character.key);
        });

        return;
      }
    }
  });

  game.world.on('end-contact', (contact) => {
    let bodyA = contact.getFixtureA().getBody();
    let bodyB = contact.getFixtureB().getBody();

    let isGround = bodyA === map.body || bodyB === map.body;
    let character = findCharacter(bodyA, bodyB);

    if (isGround && character) {
      character.endContact(contact);
      return;
    }
  });


  let canvas = game.setupRender();
  document.querySelector('.game').appendChild(canvas);

  window.addEventListener('resize', () => {
    game.setupRender();
  });


  let renderTime = Date.now();

  let render = () => {
    let dt = Date.now() - renderTime;
    renderTime += dt;

    stats.begin();
    game.render(dt);
    stats.end();

    requestAnimationFrame(render);
  };

  requestAnimationFrame(render);


  let updateTime = Date.now();

  let update = () => {
    let dt = Date.now() - updateTime;
    updateTime += dt;

    game.update(Math.min(dt, game.renderSettings.maximumUpdateDt) / 1e3);
  };

  setInterval(update, 1e3 / 60);


  let controlsFolder = pane.addFolder({ title: 'Controls' });

  let controls = new Controls(controlsFolder);
  game.controls = controls;

  controls.addPlayer({ character: player1 });
  controls.addPlayer({ character: player2 });



  let renderFolder = pane.addFolder({ title: 'Rendering' });

  renderFolder.addInput(game.renderSettings, 'decimalScaling', { label: 'Decimal scaling' });
  renderFolder.addInput(game.renderSettings, 'deviceScale', { label: 'Device scale' });
  renderFolder.addInput(game.renderSettings, 'postprocessing', { label: 'Postprocessing' });
  renderFolder.addInput(game.renderSettings, 'postprocessingScaling', { label: 'Postprocessing scaling'});
  renderFolder.addButton({ title: 'Apply settings' }).on('click', () => {
    game.setupRender();
  });

  renderFolder.addSeparator();
  renderFolder.addInput(game.renderSettings, 'debug', { label: 'Show fixtures' });
  renderFolder.addInput(game.renderSettings, 'maximumUpdateDt', { label: 'Maximum update dt' });


  let audioFolder = pane.addFolder({ title: 'Audio' });

  audioFolder.addInput(game.audioSettings, 'musicVolume', { label: 'Music', min: 0, max: 1 }).on('change', (event) => {
    if (game.musicAudio) {
      game.musicAudio.volume = event.value;
    }
  });

  audioFolder.addInput(game.audioSettings, 'sfxVolume', { label: 'SFX', min: 0, max: 1 });


  let mapFolder = pane.addFolder({ title: 'Map' });

  mapFolder.addInput({ path: mapPaths[0] }, 'path', {
    label: 'Map',
    options: Object.fromEntries(mapPaths.map((path) => [path, path]))
  }).on('change', (event) => {
    GameMap.load(event.value, {
      game,
      physScale: Settings.physScale,
      resources,
      world: game.world
    }).then((_map) => {
      map = _map;
      game.setupMap(map);
      game.setupRender();
    })
  });
}


class Controls {
  constructor(folder) {
    this._folder = folder;
    this._folderItems = new Set();
    this._options = new Map();
    this._nextOptionIndex = 0;
    this._players = [];


    this.addOption({
      createController: () => new ctrl.ArrowsController(),
      display: 'gamepad',
      label: 'Arrows'
    });


    if (navigator.keyboard) {
      let indices = [];

      this.addKeyboardOptions().then((_indices) => {
        indices = _indices;
      });

      if (navigator.keyboard.addEventListener) {
        navigator.keyboard.addEventListener('layoutchange', () => {
          for (let index of indices) {
            this.deleteOption(index);
          }

          this.addKeyboardOptions().then((_indices) => {
            indices = _indices;
          });
        });
      }
    } else {
      this.addDefaultKeyboardOptions();
    }


    let gamepadIndices = [];

    window.addEventListener('gamepadconnected', (event) => {
      let index = this.addOption({
        createController: () => new ctrl.GamepadController(event.gamepad.index, () => {
          if (this._options.has(index)) {
            this.deleteOption(index);
          }
        }),
        display: 'gamepad',
        label: event.gamepad.id
      });
    });
  }

  addOption(option) {
    let index = this._nextOptionIndex++;

    this._options.set(index, option);

    for (let player of this._players) {
      if (player.optionIndex < 0) {
        this.setPlayerOption(player, this.findFreeOption());
        break;
      }
    }

    this.setupFolder();

    return index;
  }

  addPlayer(player) {
    player.controller = null;
    player.optionIndex = -1;

    this._players.push(player);
    this.setPlayerOption(player, this.findFreeOption());

    this.setupFolder();
  }

  deleteOption(index) {
    for (let player of this._players) {
      if (player.optionIndex === index) {
        this.setPlayerOption(player, -1);
      }
    }

    this._options.delete(index);

    for (let player of this._players) {
      if (player.optionIndex < 0) {
        this.setPlayerOption(player, this.findFreeOption());
      }
    }

    this.setupFolder();
  }

  findFreeOption() {
    let takenOptionIndices = new Set(this._players.map((player) => player.optionIndex));

    for (let optionIndex of this._options.keys()) {
      if (!takenOptionIndices.has(optionIndex)) {
        return optionIndex;
      }
    }

    return -1;
  }

  getPlayerController(playerIndex) {
    return this._players[playerIndex].controller;
  }

  setPlayerOption(player, optionIndex) {
    if (player.controller) {
      player.controller.stop();
    }

    if (optionIndex >= 0) {
      player.controller = this._options.get(optionIndex).createController();
      player.controller.start(player.character.response);
    }

    player.optionIndex = optionIndex;
  }

  setupFolder() {
    for (let item of this._folderItems) {
      item.dispose();
    }

    this._folderItems.clear();

    for (let playerIndex = 0; playerIndex < this._players.length; playerIndex++) {
      let player = this._players[playerIndex];

      let item = this._folder.addInput({ value: player.optionIndex }, 'value', {
        label: `Player ${playerIndex + 1}`,
        options: {
          'Disabled': -1,
          ...Object.fromEntries(Array.from(this._options.entries()).map(([optionIndex, option]) => [option.label, optionIndex]))
        }
      });

      item.on('change', (event) => {
        this.setPlayerOption(player, event.value);
      });

      this._folderItems.add(item);
    }
  }

  async addKeyboardOptions() {
    let map;

    try {
      map = await navigator.keyboard.getLayoutMap();
    } catch (err) {
      return this.addDefaultKeyboardOptions();
    }

    let wasd = map.get('KeyW') === 'w';

    let index = this.addOption({
      createController: () => new ctrl.KeyboardController({
        left: map.get('KeyA'),
        up: map.get('KeyW'),
        right: map.get('KeyD'),
        down: map.get('KeyS'),
        fire: ' '
      }),
      display: wasd ? 'qwerty' : 'azerty',
      label: `Automatic (${wasd ? 'WASD' : 'ZQSD'})`
    });

    return [index];
  }

  addDefaultKeyboardOptions() {
    let index1 = this.addOption({
      createController: () => new ctrl.ZQSDController(),
      display: 'azerty',
      label: 'ZQSD'
    });

    let index2 = this.addOption({
      createController: () => new ctrl.WASDController(),
      display: 'qwerty',
      label: 'WASD'
    });

    return [index1, index2];
  }
}
