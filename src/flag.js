import * as planck from 'planck-js';

import * as util from './util';


export class Flag {
  constructor(options) {
    this.game = options.game;
    this.key = options.key;
    this.physScale = options.physScale;
    this.spawnPoint = options.spawnPoint;
    this.tileset = options.tileset;
    this.world = options.world;

    this.carrier = null;
    this.isAtSpawnPoint = true;

    this.body = this.world.createDynamicBody({
      allowSleep: false,
      position: this.spawnPoint,
      gravityScale: 0
    });

    this.body.createFixture({
      filterCategoryBits: 0b1000,
      filterMaskBits: 0b0010,
      isSensor: true,
      shape: planck.Box(0.5, 0.5)
    });

    this.renderFrame = this.tileset.renderAnimation({
      loop: true
    });
  }

  get renderPosition() {
    if (this.carrier) {
      let carrierPos = this.carrier.body.getPosition();

      return {
        x: carrierPos.x / this.physScale,
        y: (carrierPos.y - this.carrier.size.height) / this.physScale
      };
    } else {
      let position = this.body.getPosition();

      return {
        x: position.x / this.physScale,
        y: (position.y - 0.3) / this.physScale
      };
    }
  }

  destroy() {
    this.world.destroyBody(this.body);
  }

  drop(position) {
    this.carrier = null;

    setTimeout(() => {
      this.body.setActive(true);
      this.body.setPosition(planck.Vec2(position.x, position.y));
    });
  }

  reset() {
    this.carrier = null;
    this.isAtSpawnPoint = true;

    this.body.setActive(true);
    this.body.setPosition(this.spawnPoint);
  }

  setCarrier(carrier) {
    this.carrier = carrier;
    this.isAtSpawnPoint = false;

    this.body.setActive(false);
  }

  render(ctx) {
    if (this.carrier) {
      this.tileset.renderTile(ctx, 0, this.renderPosition);
    } else {
      this.renderFrame(ctx, this.renderPosition);
    }

    if (this.game.renderSettings.debug) {
      util.renderFixture(ctx, this.body.getFixtureList(), this.physScale, 'blue');
    }
  }
}


export class FlagBase {
  constructor(options) {
    this.key = options.key;

    this.body = options.world.createBody({
      position: options.position
    });

    this.body.createFixture({
      filterCategoryBits: 0b10000,
      filterMaskBits: 0b00010,
      isSensor: true,
      shape: planck.Box(0.5, 0.5)
    });
  }
}
