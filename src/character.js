import planck from 'planck-js';

import { Bullet } from './bullet';
import { ParticleAnimation } from './particle';
import * as util from './util';


export default class Character {
  constructor(options) {
    this.game = options.game;
    this.key = options.key;
    this.physScale = options.physScale;
    this.resources = options.resources;
    this.settings = options.settings;
    this.tilesets = options.tilesets;

    this.body = options.world.createDynamicBody({
      allowSleep: false,
      fixedRotation: true,
      position: { x: 0, y: 0 }
    });


    this.dirLeft = false;
    this.firing = false;
    this.grounded = false;
    this.groundContacts = 0;
    this.jumping = false;
    this.lastFireTime = 10;
    this.lastGroundTime = 10;
    this.lastJumpTime = 10;
    this.running = false;

    this.carriedFlag = null;
    this.health = 3;

    this.size = {
      width: this.tilesets.idle.tileSize.width * options.physScale,
      height: this.tilesets.idle.tileSize.height * options.physScale
    };

    let hitbox = options.hitbox || {
      width: this.size.width * 0.8,
      height: this.size.height
    };

    this.fixture = this.body.createFixture({
      density: 1,
      filterCategoryBits: 0b00010,
      filterMaskBits: 0b11101,
      shape: planck.Box(hitbox.width / 2, hitbox.height / 2)
    });

    this.sensorFixture = this.body.createFixture({
      filterCategoryBits: 0b0010,
      filterMaskBits: 0b0001,
      isSensor: true,
      shape: planck.Box(hitbox.width / 2 * 0.8, 0.1, planck.Vec2(0, 0.5))
    });
  }

  get controller() {
    return this.game.controls.getPlayerController(this.key);
  }

  get currentTileset() {
    return this.grounded
      ? this.running
        ? this.tilesets.run
        : this.tilesets.idle
      : this.tilesets.jump;
  }

  get spawnPoints() {
    return this.game.map.spawnPoints.filter((point) => point.key === this.key);
  }

  get response() {
    return {
      left:     () => { this.setRunning(true); },
      leftEnd:  () => { if (this.dirLeft) this.setIdle(); },
      up:       () => { this.jumping = true; },
      upEnd:    () => { this.jumping = false; },
      right:    () => { this.setRunning(false); },
      rightEnd: () => { if (!this.dirLeft) this.setIdle(); },
      down:     () => {},
      downEnd:  () => {},
      fire:     () => { this.firing = true; },
      fireEnd:  () => { this.firing = false; }
    };
  }

  beginContact(contact) {
    let fixtureA = contact.getFixtureA();
    let fixtureB = contact.getFixtureB();

    if (fixtureA === this.sensorFixture || fixtureB === this.sensorFixture) {
      this.groundContacts++;
      this.grounded = this.groundContacts > 0;
    }

    if (this.groundContacts === 1) {
      this.lastGroundTime = 0;
    }
  }

  endContact(contact) {
    let fixtureA = contact.getFixtureA();
    let fixtureB = contact.getFixtureB();

    if (fixtureA === this.sensorFixture || fixtureB === this.sensorFixture) {
      this.groundContacts--;
      this.grounded = this.groundContacts > 0;
    }


    let velocity = this.body.getLinearVelocity();

    if (this.running && velocity.x === 0) {
      this.running = false;
      this.setRunning();
    }
  }

  hit() {
    this.health--;
    this.game.setHealthbar(this.key, this.health);

    let controller = this.controller;

    if (this.health === 0) {
      this.game.particles.add(
        new ParticleAnimation({
          game: this.game,
          position: this.body.getPosition(),
          physScale: this.physScale,
          tileset: this.resources.explosionTileset
        })
      );

      if (this.carriedFlag) {
        this.carriedFlag.drop(this.body.getPosition());
        this.carriedFlag = null;
      }

      if (this.game.post) {
        this.game.post.shake(4);
      }

      this.game.playAudio(this.resources.playerDeathAudio);

      if (controller) {
        controller.vibrate(1, 600);
      }

      setTimeout(() => {
        this.respawn();
      });
    } else if (controller) {
      controller.vibrate(0.8, 300);
    }

    for (let character of this.game.characters) {
      if (character.key !== this.key) {
        let controller = character.controller;

        if (controller) {
          controller.vibrate(0.5, 300);
        }
      }
    }
  }

  reset() {
    this.carriedFlag = null;
    this.respawn();
  }

  resetMap() {
    this.groundContacts = 0;
    this.reset();
  }

  respawn() {
    this.health = 3;

    this.game.setHealthbar(this.key, this.health);

    this.body.setLinearVelocity({ x: 0, y: 0 });
    this.body.setPosition(util.chooseRandomly(this.spawnPoints));
  }

  takeFlag(flag) {
    this.carriedFlag = flag;
    this.game.playAudio(this.resources.pickupFlagAudio);
  }


  render(ctx) {
    let value = Math.round(Date.now() / 100) % this.currentTileset.tileCount;
    let { image, x: imageX, y: imageY } = this.currentTileset.getTile(value);
    let imageSize = this.currentTileset.tileSize;

    let position = this.body.getPosition();
    let targetX = (position.x - this.size.width / 2) / this.physScale;
    let targetY = (position.y - this.size.height / 2) / this.physScale;

    ctx.save();

    if (this.dirLeft) {
      ctx.translate(2 * targetX + imageSize.width, 0);
      ctx.scale(-1, 1);
    }

    ctx.drawImage(image,
      imageX, imageY,
      imageSize.width, imageSize.height,
      targetX, targetY,
      imageSize.width, imageSize.height
    );

    ctx.restore();

    if (this.game.renderSettings.debug) {
      util.renderFixture(ctx, this.fixture, this.physScale);
      util.renderFixture(ctx, this.sensorFixture, this.physScale);
    }
  }

  jump() {
    if (this.grounded && this.lastGroundTime > 0.030) {
      this.grounded = false;
      this.lastJumpTime = 0;

      this.body.applyLinearImpulse(planck.Vec2(0, -this.settings.jumpVelocity * this.body.getMass()), this.body.getWorldCenter());
      this.game.playAudio(this.resources.jumpAudio);
    }
  }

  setIdle() {
    if (!this.running) {
      return;
    }

    this.running = false;

    let velocity = this.body.getLinearVelocity();
    let impulse = (-velocity.x) * this.body.getMass();

    this.body.applyLinearImpulse(planck.Vec2(impulse, 0), this.body.getWorldCenter());
  }

  setRunning(dirLeft = this.dirLeft) {
    if (this.running && this.dirLeft === dirLeft) {
      return;
    }

    this.dirLeft = dirLeft;
    this.running = true;

    let velocity = this.body.getLinearVelocity();
    let impulse = (-velocity.x + this.settings.runVelocity * (dirLeft ? -1 : 1)) * this.body.getMass();

    this.body.applyLinearImpulse(planck.Vec2(impulse, 0), this.body.getWorldCenter());
  }

  fire() {
    let position = this.body.getPosition();
    let velocity = this.body.getLinearVelocity();

    this.game.bullets.add(new Bullet({
      image: this.bulletImage,
      position: {
        x: position.x + this.size.width / 2 * (this.dirLeft ? -1 : 1),
        y: position.y + this.size.height * 0.2
      },
      velocity: {
        x: velocity.x + this.settings.bulletVelocity * (this.dirLeft ? -1 : 1),
        y: velocity.y
      },

      game: this.game,
      physScale: this.physScale,
      resources: this.resources,
      world: this.game.world
    }, this.game));

    this.game.playAudio(this.resources.fireAudio);
  }

  update(dt) {
    let time = Date.now();

    this.lastFireTime += dt;
    this.lastGroundTime += dt;
    this.lastJumpTime += dt;

    if (this.firing && this.lastFireTime > this.settings.firingInterval) {
      this.lastFireTime = 0;
      this.fire();
    }


    if (this.grounded && this.jumping) {
      this.jump();
    }
  }
}
