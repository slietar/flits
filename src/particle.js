export class ParticleAnimation {
  constructor(options) {
    this.game = options.game;
    this.physScale = options.physScale;
    this.position = options.position; /* center in phys coords */
    this.tileset = options.tileset;

    this.renderFrame = this.tileset.renderAnimation({
      position: {
        x: options.position.x / this.physScale,
        y: options.position.y / this.physScale
      }
    });
  }

  render(ctx) {
    let done = this.renderFrame(ctx);

    if (done) {
      this.game.particles.delete(this);
    }

    /* ctx.beginPath();
    ctx.arc(this.position.x / this.physScale, this.position.y / this.physScale, 3, 0, Math.PI * 2);
    ctx.fill(); */
  }
}

