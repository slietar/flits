# Flits


## Drawing library

```js
let renderer = new lc.Renderer(canvas);

let lampTexture = new lc.Texture(await lc.loadImage('./assets/lamp.png'));
let lampProfile = new lc.Profile({
  fragment: new lc.Shader(await lc.loadShader('./shaders/lamp.frag')),
  vertex:   new lc.Shader(await lc.loadShader('./shaders/lamp.vert'))
});

let lamp = new lc.Object(lampProfile);

lamp.uniforms.texture = lampTexture;
lamp.position = { x: 0, y: 0 };

renderer.attach(lamp);


function render(time) {
  lamp.varyings.time = time;

  renderer.render();
}
```


## Shader engine

```js
let renderer = new lc.Renderer(canvas);
let shader = new lc.Shader(glslify('./shader.frag'));

let circle = new lc.Shape.Circle({
  shader,
  varyings: [...]
});

circle.position.x = Math.random() * 2 - 1;
circle.position.y = Math.random() * 2 - 1;
circle.radius = 0.2;

circle.varyings.time = time;

renderer.attach(circle);
renderer.render();
```

```glsl
precision mediump float;

varying vec2 vPosition;
varying vec3 vTime;

void main() {
  gl_FragColor = vec4(cos(vTime) * 0.5 + 0.5, vPosition.x /* radius */, 1.0, 1.0);
}

```

