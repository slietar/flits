const webpack = require('webpack');

module.exports = {
  context: __dirname,
  mode: 'development',
  devtool: 'inline-source-map',

  entry: {
    main: './src/index.js',
  },
  output: {
    path: __dirname + '/build',
    filename: '[name].bundle.js'
  }
};

